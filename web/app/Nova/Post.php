<?php

namespace App\Nova;

use Arsenaltech\NovaTab\NovaTab;
use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;
use Fourstacks\NovaRepeatableFields\Repeater;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Markdown;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use Manogi\Tiptap\Tiptap;

class Post extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Post';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            new NovaTab('Meta', [
                Boolean::make('Published'),
                Date::make('Date')
                    ->format('DD.MM.YYYY')
                    ->sortable(),
                TextWithSlug::make('Title')
                    ->slug('Slug')
                    ->sortable(),
                Slug::make('Slug'),
                BelongsTo::make('User')
                    ->sortable(),
            ]),
            new NovaTab('Content', [
                Tiptap::make('Excerpt')
                    ->hideFromIndex()
                    ->buttons([
                        'bold',
                        'italic',
                        'code',
                        'strike',
                    ]),
                Markdown::make('Body')
                    ->alwaysShow(),
            ]),
            BelongsToMany::make('Projects')
                ->sortable(),
            BelongsToMany::make('Tools')
                ->sortable(),
            BelongsToMany::make('Categories')
                ->sortable(),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
