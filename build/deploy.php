<?php
/**
 * This deploy task is for running out of a ci for pipeline deployments.
 * It's build for laravel applications
 */

namespace Deployer;

require 'recipe/laravel.php';

set('application', 'JC');

// Project repository
set('repository', 'git@gitlab.com:jchristlieb/v3.janchristlieb.de');

set('git_tty', true);

// Laravel shared dirs
set('shared_dirs', [
    'storage',
]);

// Laravel shared file
set('shared_files', [
    '.env',
]);

host('staging')
    ->stage('staging')
    ->hostname('185.227.114.236')
    ->user('p461591')
    ->identityFile('~/.ssh/id_rsa')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->set('http_user', 'p461591')
    ->set('writable_mode', 'chmod')
    ->set('deploy_path', '/home/www/p461591/html/staging.janchristlieb.de');


host('production')
    ->stage('production')
    ->hostname('185.227.114.236')
    ->user('p461591')
    ->identityFile('~/.ssh/id_rsa')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->set('http_user', 'p461591')
    ->set('writable_mode', 'chmod')
    ->set('deploy_path', '/home/www/p461591/html/production.janchristlieb.de');


task('deploy:update_code', function () {
    $artifactFile = 'artifact.tar.gz';
    runLocally('tar -czvf ' . $artifactFile . ' ./../web/');
    upload($artifactFile, '{{release_path}}/' . $artifactFile);
    run('cd {{release_path}} && tar xvf {{release_path}}/' . $artifactFile . ' --strip 1');
    run('cd {{release_path}} && rm {{release_path}}/' . $artifactFile);
});

/**
 * Main task
 */
desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'artisan:storage:link',
    'artisan:view:clear',
    'artisan:config:cache',
    'artisan:optimize',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
]);
before('deploy:symlink', 'deploy:public_disk');
after('deploy', 'success');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.
//before('deploy:symlink', 'artisan:migrate');
