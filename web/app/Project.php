<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Project extends Model implements HasMedia
{

    use HasMediaTrait;

    protected $casts = [
        'highlights' => 'array',
    ];

    public function setImagesAttribute()
    {}

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumbnail')
            ->fit(Manipulations::FIT_CROP, 64, 64);

        $this->addMediaConversion('200x200')
            ->fit(Manipulations::FIT_CROP, 200, 200)
            ->greyscale();

        $this->addMediaConversion('805x345')
            ->fit(Manipulations::FIT_CONTAIN, 805, 345)
            ->withResponsiveImages();
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('project-image');
    }

    public function getProjectExcerpt($body)
    {
        $offset = 100;
        $bodylength = strlen($body);

        if ($offset > $bodylength) {
            return $body;
        }

        $position = strpos($body, ' ', $offset);
        $excerpt = substr($body,0,$position) . ' ...';

        return $excerpt;
    }

    // a project belongs to many tools
    public function tools()
    {
        return $this->belongsToMany(Tool::class);
    }

    // a project belongs to many posts
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    // a project belongs to one category
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
