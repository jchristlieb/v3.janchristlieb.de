<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('snippets.head')
<body>
@include('snippets.nav')
<div class="container-sm">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="jumbotron m-0">
                <h1 class="text-center text-neutral-100">About</h1>
                <hr class="bg-primary-70 hr-100 hr-medium">
                <div class="text-center mt-4 mb-4 p-0">
                    <img src="/svg/jc_about.jpg" alt="" class="img-fluid img-about mt-4">
                </div>
            </div>
        </div>
        <div class="col-10 col-sm-8">
            <p class="secondary-content text-justify">
                Hi. My name is Jan. I am on a journey into web development with a strong desire to master the art of crafting
                digital products.
            </p>
            <p class="secondary-content text-justify">
                Do you know this feeling of creating stuff? A product you are proud of because you worked hard
                for it and it works like expected, maybe even better. For me, this is a great source of motivation. It
                keeps me going, it makes it easier to deal with all the small and big barriers
                along the way. It creates a desire to get going and keep learning.
            </p>

            <p class="secondary-content text-justify">
                Whether you are interested in collaboration, feedback to some content on this site, or just want to say hello
                - feel free to <a class="text-primary-80" href="/imprint">get in touch</a>.
            </p>
            <p class="secondary-content">
                Cheers and thanks for passing by.
            </p>
        </div>
    </div>
</div>
@yield('main')
@include('snippets.footer')
</body>
</html>
