<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use sixlive\ParsedownHighlight;

class Post extends Model
{
    protected $casts = [
      'sources' => 'array',
        'date' => 'date',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function readingTime()
    {
        return ceil(str_word_count($this->body) / 250);
    }

    public function getBodyAsHtml()
    {
        $parsedown = app(ParsedownHighlight::class);

        return $parsedown->text($this->body);
    }

    // a post belongs to one user
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // a post belongs to many categories
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    // a post belongs to many tools
    public function tools()
    {
        return $this->belongsToMany(Tool::class);
    }

    // a post belongs to many projects
    public function projects()
    {
        return $this->belongsToMany(Project::class);
    }
}
