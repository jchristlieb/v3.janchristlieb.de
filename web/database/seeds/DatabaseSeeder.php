<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // create test user
        factory(\App\User::class, 1)->create([
            'name' => 'Jan',
            'email' => 'mail@janchristlieb.de',
            'email_verified_at' => now(),
            'password' => bcrypt('pw'),
            'remember_token' => str_random(10),
        ]);
        $this->command->info('test user created');

        // create project categories
        $p_categories = collect();
        collect(['Personal Project', 'Client Project'])->each(function ($p_category) use ($p_categories) {
            factory(\App\Category::class, 1)->create([
                'name' => $p_category,
                'slug' => str_replace(' ','-', strtolower($p_category)),
            ]);
        });
        $this->command->info('Project categories created');

        // create categories
        $categories = collect();
        collect(['IT operation', 'Framework', 'Library', 'Database', 'Marketing', 'Language'])->each(function ($category) use ($categories){
            factory(\App\Category::class,1)->create([
                'name' => $category,
                'slug' => str_replace(' ','-', strtolower($category)),
            ]);
        });
        $this->command->info('categories created');


        // create 5 posts
        $posts = factory(\App\Post::class, 5)->create();
        $this->command->info('5 posts created');

        // create 20 tools
        $tools = factory(\App\Tool::class, 20)->create();
        $this->command->info('20 tools created');

        // create 4 projects
        $projects = factory(\App\Project::class,4)->create();
        $this->command->info('4 projects created');

        // attach 0 to 2 categories to posts
        $posts->each(function ($post) {
            $randnr = rand(1,100);
            $cat1 = \App\Category::inRandomOrder()->first()->id;
            $cat2 = \App\Category::inRandomOrder()->first()->id;

            // 75% chance that
            if ($randnr >= 25 ){
                $post->categories()->attach($cat1);
                if ($cat2 != $cat1) {
                    $post->categories()->attach($cat2);
                }
            }
        });
        $this->command->info('all posts have 0 to 2 categories');

        // attach 0 to 3 posts to a tool
        $tools->each(function ($tool) {
            $randnr = rand(1,100);
            $post1 = \App\Post::inRandomOrder()->first()->id;
            $post2 = \App\Post::inRandomOrder()->first()->id;
            $post3 = \App\Post::inRandomOrder()->first()->id;

            //50% chance that a tool has posts
            if ($randnr <= 50) {
                $tool->posts()->attach($post1);
                if ($post2 != $post1) {
                    $tool->posts()->attach($post2);
                    if ($post3 !=$post1 AND $post3 !=$post2) {
                        $tool->posts()->attach($post3);
                    }
                }
            }
        });
        $this->command->info('all tools have 0 to 3 posts');

        // attach 0 to 2 posts to projects
        $projects->each(function ($project) {

            $randnr = rand(1, 100);
            $post1 = \App\Post::inRandomOrder()->first()->id;
            $post2 = \App\Post::inRandomOrder()->first()->id;

            //50% chance that a project has posts
            if ($randnr <= 50) {
                $project->posts()->attach($post1);

                // ~ 25% chance that a project has 2 posts
                if ($randnr <= 25 AND $post2 != $post1) {
                    $project->posts()->attach($post2);
                }
            }
        });
        $this->command->info('all projects have 0 to 2 posts');

        // attach 1 to 3 tools to projects
        $projects->each(function ($project) {
            $randnr = rand(1,100);
            $tool1 = \App\Tool::inRandomOrder()->first()->id;
            $tool2 = \App\Tool::inRandomOrder()->first()->id;
            $tool3 = \App\Tool::inRandomOrder()->first()->id;

            // each project is attached to a tool
            $project->tools()->attach($tool1);

            // ~ 50% of projects are attached to 2 tools
            if ($randnr <= 75 AND $tool1 != $tool2) {
                $project->tools()->attach($tool2);

                // ~ 25% of projects are attached to 3 tools
                if ($randnr <=50 AND $tool2 != $tool3 AND $tool1 != $tool3) {
                    $project->tools()->attach($tool3);
                }
            }
        });
        $this->command->info('all projects have 1 to 3 tools');
    }
}
