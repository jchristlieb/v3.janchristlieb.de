@extends('layouts.master')
@section('title','Tools')
@section('subtitle')

    Hand-picked resources.

    @endsection
@section('main')
    <div class="container-md">
        <div class="row">
            <div class="mt-4 mb-4 tool-category col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
                <div class="text-center">
                    @foreach($filters as $filter)
                        <a href="/tools?category={{ $filter->slug }}" role="button"
                           class="btn btn-outline-primary mr-2 mb-3">{{ $filter->name }}</a>
                    @endforeach
                    <a href="/tools" role="button" class="btn btn-outline-primary mr-2 mb-3">All categories</a>
                </div>
            </div>
            @foreach($categories as $category)
                <div class="mt-4 mb-4 tool-category col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
                    <h2>{{ $category->name }}</h2>
                    <hr class="bg-primary-70 hr-thick" width="80px" align="left">
                    @foreach($category->tools->sortBy('slug') as $tool)
                        <div class="tool-teaser mt-4 mb-4">
                            <div class="card shadow-hover-effect">
                                <a href="/tools/{{ $tool->slug }}">
                                    <div class="card-body row">
                                        <div class="col fixed-100">
                                            @if($tool->getFirstMediaUrl('tool-image', '75x75') != '')
                                                <img src="{{$tool->getFirstMediaUrl('tool-image', '75x75') }}" alt="" class="img-fluid">
                                                @else
                                                <img class="img-fluid img-tool-teaser">
                                            @endif
                                        </div>
                                        <div class="col">
                                            <h3 class="text-neutral-80">{{ $tool->name }}</h3>
                                            <div class="wysiwyg-tertiary-content">
                                            {{--teaser = first 50 characters of $body until the end of sentence.--}}
                                            <?php preg_match('/(^.{0,30}[^!?.]*.).*$/s', $tool->body, $teaser);?>
                                            {!!    $teaser[1] !!}
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
@endsection