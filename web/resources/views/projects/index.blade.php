@extends('layouts.master')
@section('title', 'Projects')
@section('subtitle')

    It starts as an idea and ends with a product.

@endsection
@section('main')

    <div class="container-md">
        <div class="text-center mb-4">
            @foreach($filters as $filter)
                <a href="/projects?category_id={{ $filter->id }}" role="button"
                   class="btn btn-outline-primary mr-2 mb-3">{{ $filter->name .'s'}}</a>
            @endforeach
            <a href="/projects" role="button" class="btn btn-outline-primary mr-2 mb-3">All Projects</a>
        </div>

        <div class="row">
            @foreach($projects as $project)
                <div class="col-12">
                    <a class="no-underline" href="/projects/{{$project->slug}}">
                        <div class="m-4 p-0 project-teaser d-flex shadow-hover-effect">
                            <div class="fixed-200 d-none d-md-block">
                                <img class="img-project-teaser"
                                     src="{{$project->getFirstMediaUrl('project-image', '200x200')}}"
                                     alt="{{$project->name}}">
                            </div>
                            <div class="col teaser-body bg-white">
                                <div class="p-3">
                                    <h2 class="text-primary-80 pt-3 pb-3">{{$project->name}}</h2>
                                    <div class="wysiwyg">
                                        {!! $project->getProjectExcerpt($project->body)!!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>

        {{--<ul class="navbar-nav">--}}
        {{--@foreach($projects as $project)--}}
        {{--<li><a href="/projects/{{$project->slug}}" alt="{{$project->name}}">--}}
        {{--<div class="project-teaser d-flex shadow-hover-effect mt-2 mb-2">--}}
        {{--<div class="d-none d-sm-block">--}}
        {{--<img class="img-project-teaser"--}}
        {{--src="{{$project->getFirstMediaUrl('project-image', '200x200')}}" alt="">--}}
        {{--</div>--}}
        {{--<div class="flex-fill p-3">--}}
        {{--<h2 class="mb-3">{{$project->name}}</h2>--}}
        {{--@foreach($project->tools as $tool)--}}
        {{--<a href="/tools/{{ $tool->slug }}"--}}
        {{--class="badge badge-primary mr-2 mb-2">{{ $tool->name }}</a>--}}
        {{--@endforeach--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</a></li>--}}

        {{--@endforeach--}}
        {{--</ul>--}}
    </div>

@endsection

