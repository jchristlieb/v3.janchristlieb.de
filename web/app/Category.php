<?php

namespace App;

use function foo\func;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    // a category belongs to many posts
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    // a category has many tools
    public function tools()
    {
        return $this->hasMany(Tool::class);
    }

    // a category has many projects
    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
