<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('snippets.head')
<body>
@include('snippets.nav')
<div class="container-md">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="jumbotron m-0">
                <h1 class="text-center text-neutral-90">@yield('title')</h1>
                <hr class="bg-primary-70 hr-100 hr-medium">
                <p class="subtitle text-neutral-80 primary-content text-center">@yield('subtitle')</p>
            </div>
        </div>
    </div>
</div>
</div>
@yield('main')
@include('snippets.footer')
</body>
</html>
