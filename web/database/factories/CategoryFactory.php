<?php

use Faker\Generator as Faker;

$factory->define(App\Category::class, function (Faker $faker) {
    $categories = ['IT operation', 'Framework', 'Library', 'Database', 'Marketing', 'Language'];
    return [
        'name' => $name = array_rand($categories,1),  // pick a random key from array
        'slug' => str_replace(' ','-', strtolower($name)),
        'description' => $faker->paragraphs(rand(1,3), true),
    ];
});