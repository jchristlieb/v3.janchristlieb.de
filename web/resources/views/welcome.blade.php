@extends('layouts.master')

@section('title', 'A Journey')
@section('subtitle', 'Sharing my experiences while traveling into web development. Feel free to look around.')
@section('main')
    <div class="container-sm">
        <div class="row">
            <div class="col-12">
                <a class="no-underline" href="/journal">
                    <div class="mt-4 mb-4 p-0 d-flex section-teaser shadow-hover-effect">
                        <div class="fixed-200 d-none d-sm-block">
                            <img class="img-teaser" src="/svg/writing-min.jpg" alt="">
                        </div>
                        <div class="col teaser-body bg-white">
                            <div class="p-4">
                                <h2 class="text-primary-80 pt-3 pb-3">Journal</h2>
                                <p class="secondary-content">I mainly write about technical challenges I face and
                                    solutions I find.</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12">
                <a class="no-underline" href="/projects">
                    <div class="mt-4 mb-4 p-0 d-flex section-teaser shadow-hover-effect">
                        <div class="fixed-200 d-none d-sm-block">
                            <img class="img-teaser" src="/svg/mockups-min.jpg" alt="">
                        </div>
                        <div class="col teaser-body bg-white">
                            <div class="p-4">
                                <h2 class="text-primary-80 pt-3 pb-3">Projects</h2>
                                <p class="secondary-content">Getting stuff done. A showcase of private and client work
                                    alike.</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12">
                <a class="no-underline" href="/tools">
                    <div class="mt-4 mb-4 p-0 d-flex section-teaser shadow-hover-effect">
                        <div class="fixed-200 d-none d-sm-block">
                            <img class="img-teaser" src="/svg/tools-min.jpg" alt="">
                        </div>
                        <div class="col teaser-body bg-white">
                            <div class="p-4">
                                <h2 class="text-primary-80 pt-3 pb-3">Tools</h2>
                                <p class="secondary-content">Any craftsman needs some tools. Here you will find my
                                    toolkit</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>

@endsection
