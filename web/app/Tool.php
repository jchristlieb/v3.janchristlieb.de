<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Tool extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $casts = [
        'links' => 'array',
    ];

    public function setImagesAttribute()
    {}


    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumbnail')
            ->fit(Manipulations::FIT_CROP, 64, 64);

        $this->addMediaConversion('100x100')
            ->fit(Manipulations::FIT_CROP, 100, 100)
            ->background('F0F4F8');

        $this->addMediaConversion('75x75')
            ->fit(Manipulations::FIT_CROP, 75, 75);
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('tool-image');
    }

    // a tool belongs to one category
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    // a tool belongs to many posts
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    // a tool belongs to many projects
    public function projects()
    {
        return $this->belongsToMany(Project::class);
    }

    public function scopeFilter($query, $filter)
    {
        if($slug = $filter['category']) {
            $categories->where('slug', $slug);
        }

    }
}
