@extends('layouts.master')
@section('title', 'Journal')
@section('subtitle')

@section('main')

    <div class="container-sm">
        <div class="row">
            @foreach($posts as $post)
                <div class="post-teaser mb-2">
                    <a class="no-underline" href="/journal/{{$post->slug}}" alt="{{$post->title}}">
                        <p class="text-neutral-50 mb-2">{{$post->date->format('M d, Y')}} / {{$post->readingTime()}} min
                            read</p>
                        <h3 class="display-5 text-primary-80">{{$post->title}}</h3>
                        <div class="wysiwyg">
                            {!! $post->excerpt !!}
                        </div>
                        <hr>
                        @foreach( $post->categories as $category)
                            <span class="hash bg-primary-10 text-neutral-50 p-1 mr-2"><i
                                        class="fal fa-hashtag text-neutral-50 pr-1 mb-3"></i>{{ $category->name }}</span>
                        @endforeach
                        @foreach( $post->tools as $tool)
                            <span class="hash bg-primary-10 text-neutral-50 p-1 mr-2"><i
                                        class="fal fa-hashtag text-neutral-50 pr-1 mb-3"></i>{{ $tool->name }}</span>
                        @endforeach
                    </a>
                </div>
            @endforeach
        </div>
    </div>

@endsection