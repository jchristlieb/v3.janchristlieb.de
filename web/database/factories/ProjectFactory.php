<?php

use Faker\Generator as Faker;

$factory->define(App\Project::class, function (Faker $faker) {
    $name = $faker->name;
    return [
        'name' => $name,
        'slug' => str_replace(' ','-', strtolower($name)),
        'body' => $faker->paragraphs(rand(1,3), true),
        'url' => $faker->url,
        'repository' => $faker->url,
        'category_id' => \App\Category::where('id', rand(1,2))->first()->id, // personal or client project
    ];
});
