<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->text('body');
            $table->string('highlights')->nullable();
            $table->string('url');
            $table->string('repository')->nullable();
            $table->timestamps();
        });

        Schema::create('project_tool', function (Blueprint $table) {
            $table->integer('project_id');
            $table->integer('tool_id');
            $table->primary(['project_id', 'tool_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
        Schema::dropIfExists('project_tool');
    }
}
