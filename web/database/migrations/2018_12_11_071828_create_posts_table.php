<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('body');
            $table->string('sources')->nullable();
            $table->integer('user_id');
            $table->timestamps();
        });

        Schema::create('post_tool', function (Blueprint $table) {
            $table->integer('post_id');
            $table->integer('tool_id');
            $table->primary(['post_id', 'tool_id']);
        });

        Schema::create('post_project', function (Blueprint $table) {
            $table->integer('post_id');
            $table->integer('project_id');
            $table->primary(['post_id', 'project_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
        Schema::dropIfExists('post_tool');
        Schema::dropIfExists('post_project');
    }
}
