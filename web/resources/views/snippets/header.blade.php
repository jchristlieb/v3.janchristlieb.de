<div class="container-fluid bg-primary-50">
    <div class="row p-4 justify-content-center">
        <div class="col-12 col-md-8 col-sm-6">
            <div class="jumbotron m-4 ">
                <h1 class="text-center text-grey-40">Journal</h1>
                <p class="subtitle text-grey-40 text-center">Sharing my experiences along the way.</p>
            </div>
        </div>
    </div>
</div>