@extends('layouts.master')

@section('title', 'Legal notice')

@section('main')
    <div class="container-sm">
        <div class="row">
            <div class="col-12">
                <h2 class="">Liability and information</h2>
                <p class="primary-content">According to §5 TMG</p>
                <p class="secondary-content">Jan Christlieb<br>
                    Schwartzkopffstraße 4<br>
                    10115 Berlin<br></p>
                <p class="secondary-content">Mobil: 0176 / 99983308<br>
                    Email: mail [at] janchristlieb [dot] de<br>
                    USt-IdNr.: DE305359246</p>
            </div>
            <div class="col-12">
                <h2 class="mt-4">Responsible for content</h2>
                <p class="primary-content">According to §55 paragraph 2 RStV</p>
                <p class="secondary-content">Jan Christlieb<br>
                    Schwartzkopffstraße 4<br>
                    10115 Berlin<br></p>
            </div>
        </div>
    </div>
    @endsection
