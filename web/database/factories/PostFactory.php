<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Post::class, function (Faker $faker) {
    $title = $faker->words(rand(2,5), true);
    return [
        'title' => $title,
        'slug' => str_replace(' ','-', strtolower($title)),
        'published' => rand(0,1),
        'excerpt' => $faker->paragraphs(2,true),
        'body' => $faker->paragraphs(rand(3,6), true),
        'user_id' => \App\User::inRandomOrder()->first(), //pick a random user_id from an existing user
        'date' => $faker->dateTimeThisYear('now')->format('Y-m-d H:i:s'),
    ];
});
