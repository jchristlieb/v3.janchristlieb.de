<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'SiteController@index')->name('welcome');
Route::get('/journal', 'PostController@index')->name('journal');
Route::get('/journal/{post}', 'PostController@show')->name('post');
Route::get('/projects/', 'ProjectController@index')->name('projects');
Route::get('/projects/{slug}', 'ProjectController@show')->name('project');
Route::get('/tools', 'ToolController@index')->name('tools');
Route::get('/tools/{slug}', 'ToolController@show')->name('tool');
Route::view('/about/','about')->name('about');

Route::get('/data-privacy', function () {
    return view('data');
});

Route::get('/imprint', function () {
    return view('imprint');
});

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
