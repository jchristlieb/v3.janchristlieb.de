<div class="container-fluid">
    <nav class="container">
        <div class="navbar navbar-expand-sm navbar-light pl-md-0 pr-md-0 pt-3 pb-3">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent"
                 aria-controls="navbarSupportedContent"
                 aria-expanded="false" aria-label="Toggle navigation">
                <ul class="navbar-nav">
                    <li class="nav-item"><a href="/" class="nav-link">Home</a></li>
                    <li class="nav-item"><a href="/journal" class="nav-link">Journal</a></li>
                    <li class="nav-item"><a href="/projects" class="nav-link">Projects</a></li>
                    <li class="nav-item"><a href="/tools" class="nav-link">Tools</a></li>
                    <li class="nav-item"><a href="/about" class="nav-link">About</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>