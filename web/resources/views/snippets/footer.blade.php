<footer class="container-fluid bg-neutral-100 mt-4">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center pt-4 pb-2">
                <a class="navbar-brand text-neutral-10 letter-spacing-lg m-0 pb-4" href="/"><strong id="logo-color" class="text-primary-70 pr-2">jan</strong>christlieb</a>
            </div>
            <div class="col-12 text-center p-2">
                <ul class="list-inline mb-0">
                <li class="list-inline-item p-3"><a href="https://twitter.com/jchristlieb"><i class="text-primary-70 fab fa-twitter fa-2x"></i></a></li>
                <li class="list-inline-item p-3"><a href="https://github.com/jchristlieb"><i class="text-primary-70 fab fa-github fa-2x"></i></a></li>
                <li class="list-inline-item p-3"><a href="https://www.xing.com/profile/Jan_Christlieb"><i class="text-primary-70 fab fa-xing fa-2x"></i></a></li>
                </ul>
            </div>
            <div class="col-12 text-center pb-4">
                <hr class="bg-neutral-90 hr-200">
                <ul class="list-inline mb-0">
                <li class="list-inline-item p-3"><a class="text-neutral-60" href="/imprint">Legal Notice</a></li>
                <li class="list-inline-item p-3"><a class="text-neutral-60" href="/data-privacy">Data Privacy</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>


<script type="text/javascript">
    pushFooterToBottom();

    function pushFooterToBottom() {
        if ($(document).outerHeight() <= $(window).outerHeight()) {
            $('footer').addClass('push-to-bottom');
        } else {
            $('footer').removeClass('push-to-bottom');
        }
    }
</script>