@extends('layouts.article')

@section('main')
    <div class="journal container-md">
        <article class="m-4">
            <p class="subtitle text-neutral-40 mt-4 mb-2">Published {{$post->date->format('M d, Y')}}
                / {{$post->readingTime()}} min
                read</p>
            <h1 class="text-primary-80">{{$post->title}}</h1>
            {!! $post->getBodyAsHtml() !!}
        </article>
    </div>
@endsection