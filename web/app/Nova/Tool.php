<?php

namespace App\Nova;

use Arsenaltech\NovaTab\NovaTab;
use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Fourstacks\NovaRepeatableFields\Repeater;
use Inspheric\Fields\Url;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Nova;
use Manogi\Tiptap\Tiptap;

class Tool extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Tool';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            new NovaTab('Meta', [
                ID::make()
                    ->sortable(),
                TextWithSlug::make('Name','name')
                    ->slug()
                    ->sortable(),
                Slug::make('Slug', 'slug'),
                Url::make('Manual')
                    ->label('Documentation')
                    ->rules('nullable', 'url')
                    ->clickable()
                    ->clickableOnIndex(),
                Images::make('Logo', 'tool-image')
                    ->thumbnail('thumbnail'),
            ]),
            new NovaTab('Body', [
                Tiptap::make('Body')
                    ->hideFromIndex()
                    ->buttons([
                        'bold',
                        'italic',
                        'code',
                        'link',
                        'strike',
                        'underline',
                    ]),
            ]),
            new NovaTab('Relations', [
                BelongsTo::make('Category'),
                BelongsToMany::make('Posts'),
            ]),
            new NovaTab('Links', [
                Repeater::make('Links','links')
                    ->addField([
                        'label' => 'Text',
                        'name' => 'text'
                    ])
                    ->addField([
                        'label' => 'Url',
                        'name' => 'url'
                    ])
                    ->hideFromIndex(),
            ]),


        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
