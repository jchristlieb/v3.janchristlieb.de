@extends('layouts.master')
@section('title', $project->name)
@section('main')
    @if($project->getFirstMediaUrl('project-image','805x345') != '')
        <div class="container-md d-flex justify-content-center mb-4">
            <div class="fake-browser-ui mb-4">
                <div class="frame">
                    <span class="bt-1"></span>
                    <span class="bt-2"></span>
                    <span class="bt-3"></span>
                </div>
                <div>
                    <img class="img-fluid rounded shadow-effect"
                         src="{{$project->getFirstMediaUrl('project-image','805x345')}}">
                </div>
            </div>
        </div>
    @endif
    <div class="container-sm">
        <div class="wysiwyg mt-4">{!! $project->body !!}</div>
            @if ($project->highlights != [])
            <h3 class="mt-4">Highlights</h3>
            <hr class="mb-4 mt-0 bg-primary-70 hr-medium" width="50px" align="left">
            <ul class="list-unstyled secondary-content">
                @foreach($project->highlights as $key => $value)
                    <li class="pb-2"><i class="fal fa-check-circle pr-3 text-primary-70"></i>{{$value['highlight']}}
                    </li>
                @endforeach
            @endif
        </ul>
        <h3 class="mt-4">Tools</h3>
        <hr class="mb-4 mt-0 bg-primary-70 hr-medium" width="50px" align="left">
        @foreach($project->tools as $tool)
            <a href="/tools/{{ $tool->slug }}"
               class="badge badge-primary mr-2 mb-2">{{ $tool->name }}</a>
        @endforeach
        <h3 class="mt-4">Links</h3>
        <hr class="m-0 bg-primary-70 hr-medium" width="50px" align="left">
        <ul class="list-unstyled secondary-content pt-4">
            <li class="pb-2"><i class="fal fa-external-link pr-3 text-primary-70"></i><a
                        href="{{ $project->url }}" target="_blank">Visit website</a></li>
            @if( $project->repository != '')
                <li class="pb-2"><i class="fab fa-github pr-3 text-primary-70"></i><a
                            href="{{ $project->repository }}" target="_blank"> Visit repository</a></li>
            @endif
        </ul>
    </div>
@endsection