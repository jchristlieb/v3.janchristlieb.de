@extends('layouts.tool')
@section('main')
    <div class="container-md">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                <div class="text-center m-4">
                    <img src="{{ $tool->getFirstMediaUrl('tool-image', '100x100') }}" alt=""
                         class="img-fluid img-tool text-center">
                </div>
                <h1 class="text-center text-neutral-100"><a class="no-underline" href="{{$tool->manual}}" target="_blank">{{ $tool->name }}</a></h1>
                <hr class="bg-primary-70 hr-100 hr-medium">
                <div class="wysiwyg text-justify">
                    {!! $tool->body !!}
                </div>
            </div>
        </div>
        <div class="row">
            @if(count($posts) >= 1)
                <div class="mt-4 mb-4 col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
                    <h2>Articles about {{$tool->name}}</h2>
                    <hr class="bg-primary-70 hr-thick" width="80px" align="left">
                    <ul class="list-unstyled secondary-content pt-4">
                        @foreach($posts as $post)
                            <li class="pb-2"><i class="fal fa-external-link pr-3 text-primary-70"></i><a
                                        href="/journal/{{ $post->slug }}" target="_blank">{{ $post->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(count($projects) >= 1)
                <div class="mt-4 mb-4 col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
                    <h2>Projects build with {{$tool->name}}</h2>
                    <hr class="bg-primary-70 hr-thick" width="80px" align="left">
                    <ul class="list-unstyled secondary-content pt-4">
                        @foreach($projects as $project)
                            <li class="pb-2"><i class="fal fa-external-link pr-3 text-primary-70"></i><a
                                        href="/projects/{{ $project->slug }}" target="_blank">{{ $project->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if($tool->links != [])
                <div class="mt-4 mb-4 col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
                    <h2>Suggested resources for {{$tool->name}}</h2>
                    <hr class="bg-primary-70 hr-thick" width="80px" align="left">
                    <ul class="list-unstyled secondary-content pt-4">
                        @foreach($tool->links as $key => $value)
                            <li class="pb-2"><i class="fal fa-external-link pr-3 text-primary-70"></i><a
                                        href="{{ $value['url'] }}" target="_blank">{{ $value['text'] }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
@endsection