<?php

use Faker\Generator as Faker;

$factory->define(App\Tool::class, function (Faker $faker) {
    $name = $faker->words(rand(1,2), true);
    $type = ['open source', 'commercial'];
    return [
        'name' => $name,
        'slug' => str_replace(' ','-', strtolower($name)),
        'body' => $faker->paragraphs(rand(2,3), true),
        'manual' => $faker->url,
        'creator' => $faker->lastName,
        'type' => $type[rand(0,1)],
        'category_id' => rand(1,6),
    ];
});
