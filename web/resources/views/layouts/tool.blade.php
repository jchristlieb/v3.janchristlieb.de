<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('snippets.head')
<body>
@include('snippets.nav')
@yield('main')
@include('snippets.footer')
</body>
</html>
