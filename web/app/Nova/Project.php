<?php

namespace App\Nova;

use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Inspheric\Fields\Url;
use Fourstacks\NovaRepeatableFields\Repeater;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Laravel\Nova\Http\Requests\NovaRequest;
use Manogi\Tiptap\Tiptap;

class Project extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Project';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            TextWithSlug::make('Name')
                ->slug()
                ->sortable(),
            Slug::make('Slug'),
            BelongsTo::make('Category'),
            Images::make('Screenshot', 'project-image')
                ->thumbnail('thumbnail'),
            Tiptap::make('Body')
                ->hideFromIndex()
                ->buttons([
                    'bold',
                    'italic',
                    'code',
                    'link',
                    'strike',
                    'underline',
                ]),
            Url::make('Url')
                ->label('Website')
                ->rules('url')
                ->clickable()
                ->clickableOnIndex(),
            Repeater::make('Highlights')
                ->hideFromIndex()
                ->addField([
                    'label' => 'Highlight',
                    'name'  => 'highlight',
                ])
                ->addButtonText('Add Highlight'),
            Url::make('Repository')
                ->label('GitHub')
                ->rules('nullable','url')
                ->clickable()
                ->clickableOnIndex(),
            BelongsToMany::make('Posts')
                ->sortable(),
            BelongsToMany::make('Tools')
                ->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
